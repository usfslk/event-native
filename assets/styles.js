'use strict';
import { StyleSheet, Dimensions } from 'react-native';
import { iOSColors, human, systemWeights } from 'react-native-typography';

var React = require('react-native');

const styles = StyleSheet.create({
  container: {
  flex: 1,
  backgroundColor: iOSColors.customGray,
  paddingHorizontal: 0,
  width: null,
  height: null,
},
modal: {
  height: height/2,
  backgroundColor: iOSColors.white,
  paddingVertical: 50,
  paddingHorizontal: 50,
  elevation: 50,
},
justify: {
  textAlign: 'justify'
},
center: {
  textAlign: 'center'
},
smalldivider: {
  marginVertical: 12.5,
},
largedivider: {
  paddingVertical: 25,
},
row: {
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'space-between',
},
timerow: {
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'space-between',
},
popuprow: {
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'flex-start',
  marginLeft: 50,
  marginVertical: 7
},
card: {
  borderWidth: 2,
  paddingHorizontal: 25,
  paddingVertical: 12.5,
  borderRadius: 7,
  marginBottom: 10,
  marginHorizontal: 10,
  backgroundColor: iOSColors.white,
  borderColor: iOSColors.white,
},
cardPressed: {
  borderWidth: 2,
  padding: 12.5,
  borderRadius: 7,
  backgroundColor: iOSColors.white,
  borderColor: iOSColors.pink,
  marginBottom: 10,
  marginHorizontal: 10
},
checkout: {
  paddingVertical: 25,
  fontWeight: 'bold',
  textAlign: 'center'
},
popup: {
  textAlign: 'center',
  fontWeight: 'bold',
  marginLeft: 25,
  fontSize: 16
},
popupblock: {
  backgroundColor: iOSColors.white,
  paddingVertical: 12.5,
  height: height / 3,
  width: 300,
  elevation: 15,
  borderRadius: 5,
},
white: {
  color: iOSColors.white
},
infoheader: {
  fontSize: 18,
  paddingTop:  50,
  textAlign: 'center',
  marginHorizontal: 20
},
loading: {
  margin: 50,
},
button:{
  marginVertical: 12.5,
  paddingHorizontal: 12.5

},
textbutton: {
  fontSize: 18,
  backgroundColor: '#fff',
  paddingHorizontal: 60,
  paddingVertical: 5,
  textAlign: 'center',
  color: iOSColors.gray,
  borderRadius: 50,
  borderWidth: 2,
  borderColor: iOSColors.customGray,
  marginHorizontal: 5
},
textinput: {
  backgroundColor: '#fff',
  borderRadius: 50,
  paddingVertical: 5,
  marginBottom: 15,
  paddingLeft: 10,
  borderWidth: 2,
  borderColor: iOSColors.customGray,
},
authbtn:{
  flexDirection : 'row',
  alignItems: 'stretch',
  justifyContent: 'space-between',
  marginVertical: 10,
},
spinner:{
  alignSelf: 'center',
  marginVertical: 25,
},
authContainer: {
  paddingHorizontal: 25,
},
authLogo: {
  fontSize: 30,
  paddingTop: 25,
  textAlign: 'center'
},
heroimg: {
  borderRadius: 10,
},
pSmall: {
  padding: 10
},
learnmore: {
  paddingVertical: 12.5
},
rowprofile: {
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'space-between',
},
rowsocial: {
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'space-between',
  paddingBottom: 12.5,
  paddingHorizontal: 12.5,
},
elementsCard: {
  elevation:0,
  paddingBottom: 5,
  marginBottom: 12.5,
  marginTop: 0,
  borderRadius: 8,
  borderWidth: 0
},
timeline: {
  flexDirection: 'row',
  alignItems: 'flex-start',
  justifyContent: 'space-between',

},
sessionRow: {
  backgroundColor: iOSColors.white,
  paddingVertical: 5,
  paddingHorizontal: 12.5,
  borderRadius: 8,
  marginVertical: 12.5,
  marginRight: 12.5,
  flex: 0, flexShrink: 1,

},
dateRow: {
  backgroundColor: iOSColors.pink,
  paddingVertical: 5,
  paddingHorizontal: 12.5,
  borderRadius: 50,
  marginHorizontal: 12.5,
  marginVertical: 12.5
},
speakerRow: {
  backgroundColor: iOSColors.white,
  paddingVertical: 5,
  paddingHorizontal: 12.5,
  marginVertical: 5,
  marginHorizontal: 25,
  borderRadius: 2,
  borderColor: iOSColors.black,
  borderBottomWidth: 5,
  elevation: 1
},
articleContainer: {
  paddingHorizontal: 25,
  paddingVertical: 12.5,
  borderRadius: 7,
  marginBottom: 10,
  marginHorizontal: 10,
  backgroundColor: iOSColors.white,
  borderColor: iOSColors.white,
},
blog: {
  paddingHorizontal: 25,
  paddingVertical: 12.5,
  marginHorizontal: 10,
  marginVertical: 25,
},
overlay: {
  ...StyleSheet.absoluteFillObject,
  backgroundColor: 'rgba(0,0,0,0.40)'
},
btnRow: {
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'space-between',
},









});
export default styles;
const { width, height } = Dimensions.get('window');
