import React, {Component} from 'react';
import { StyleSheet, View, FlatList, Text, Dimensions, Image, ScrollView, TouchableNativeFeedback, StatusBar, ImageBackground, TextInput, Keyboard} from 'react-native';
import { iOSColors, human, systemWeights } from 'react-native-typography'
import Modal from 'react-native-modalbox';
import { Button, Avatar, Divider, Header, Icon, Card} from 'react-native-elements'
import { Dropdown } from 'react-native-material-dropdown';
import firebase from 'firebase';
import fire from '../config'
import styles from '../assets/styles';

export default class Login extends Component {
static navigationOptions = { header: null };
  constructor(props) {
     super(props)
      console.ignoredYellowBox = [
      'Setting a timer'
      ];
     this.state = {
       currentUser: '',
       loading: false,
       error: null,
       UserInput: "",
       loggedIn: null,
       loggedOut: null,
       email: '', password: '',
     }
   }

   componentDidMount = () => {
    this.setState({ loading: true });
    firebase.auth().onAuthStateChanged((user) => {
      if(user) {
        const { navigate } = this.props.navigation;

        this.setState({ loggedIn: true, loggedOut: false, loading: false  }, () => {
            navigate('HomeScreen')
        });
      }
      else {
        this.setState({ loggedIn: false, loggedOut: true, loading: false });
      }
    });
   }

  componentWillUnmount(){
     this.setState({ isMounted : false });
  }

  onLogIn = () => {
    this.setState({ loading: true });
    const { email, password } = this.state;
    Keyboard.dismiss()
    firebase.auth().signInWithEmailAndPassword(email, password)
    .then(this.onSuccess.bind(this))
    .catch(() => {
      this.refs.modal.open();
      this.setState({ loading: false });
    })
  };

  onSignUp = () => {
    const { email, password } = this.state;
    this.setState({ loading: true });
    Keyboard.dismiss()
    firebase.auth().createUserWithEmailAndPassword(email, password)
    .then(this.onSuccess.bind(this))
    .catch(() => {
      this.refs.modal.open();
      this.setState({loading: false});
    })
  };

  onSuccess() {
    this.setState({
      email: '',
      password: '',
      loading: false,
      error: ''
    });
  };


render() {
  const { navigate } = this.props.navigation;
  return (
    <View style={{flex:1}}>
  <ImageBackground
      source={require('../assets/images/ticketbg.jpg')}
       style={{
             width: width,
             height: height,
             justifyContent: 'center',
             alignItems: 'center'
             }} >

          {this.state.loading ?
            <View>
              <Text style={[styles.center, systemWeights.bold]}>Loading ...</Text>
            </View>
          : null }

          {this.state.loggedOut ?
            <Image source={require('../assets/icon.png')}
              style={{width: 65, height: 65, marginVertical: 50}}
              />
          : null }


         <Modal style={styles.modal} backdropOpacity={0} style={styles.modal} position={"top"} ref={"modal"} backButtonClose={true} entry={'top'} animationDuration={300}>
           <View style={styles.smalldivider} />
          <Text>
              The username and password you entered did not match our records. Please double-check and try again.
          </Text>
           <View style={styles.smalldivider} />
         </Modal>

          {this.state.loggedOut ?
          <View style={styles.authContainer}>
             <View>
                <TextInput
                underlineColorAndroid={'transparent'}
                style={styles.textinput}
                placeholder='Email'
                value={this.state.email}
                onChangeText={email => this.setState({ email })}
                />
                <TextInput
                style={styles.textinput}
                underlineColorAndroid={'transparent'}
                secureTextEntry={true}
                placeholder='Password'
                value={this.state.password}
                onChangeText={password => this.setState({ password })}
                />
            </View>

            <View style={styles.authbtn}>
              <TouchableNativeFeedback
                style={styles.button}
                onPress={this.onLogIn}>
                <Text style={styles.textbutton}>Log in</Text>
              </TouchableNativeFeedback>

              <TouchableNativeFeedback
                style={styles.button}
                onPress={this.onSignUp}>
                <Text style={styles.textbutton}>Sign up</Text>
              </TouchableNativeFeedback>
            </View>
        </View>
        : null }

        {this.state.loggedOut ?
          <View style={{paddingTop: 25}}>
            <Text style={human.footnote}>By continuing you agree to our terms of service</Text>
          </View>
        : null }

     </ImageBackground>
    </View>
      );
    }
  }
  const { width, height } = Dimensions.get('window');
