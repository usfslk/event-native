import React from 'react';
import { StyleSheet, View, FlatList, Text, Dimensions, Image, WebView, TouchableOpacity, StatusBar, Linking,
TouchableNativeFeedback, ImageBackground
} from 'react-native';
import ReactNativeParallaxHeader from 'react-native-parallax-header';
import {withNavigation} from 'react-navigation';
import Icon from 'react-native-vector-icons/Feather';
import { iOSColors, human, systemWeights } from 'react-native-typography';
import { Button, Avatar, Divider, Header} from 'react-native-elements';
import firebase from 'firebase';
import fire from '../config'
import styles from '../assets/styles';
import Accordion from 'react-native-collapsible/Accordion';

class SpeakersScreen extends React.Component {
static navigationOptions = ({ navigation }) => {
  return { header: null }}

  constructor(props) {
    super(props)
    this.state = {
      loading: true,
      more: false,
    }
  }


componentDidMount = () => {
this.setState({loading: true})
const { currentUser } = firebase.auth();
  firebase.database().ref(`/master/${currentUser.uid}/feed/articles/`)
  .on('value', snapshot => {
  var obj = snapshot.val()
  var list = []
  var keys = []
  for(let a in obj){
      list.push(obj[a])
      keys.push(a)
  }
   this.setState({
        list:list,
        keys:keys,
        loading: false,
        loaded: true,
}, () => {
  if (this.state.list.length === 0)
      this.setState({loading: false, empty: true})
 });
});
}


render() {
  const { navigate } = this.props.navigation;
  return (
    <View style={styles.container}>
      <Text style={[human.subhead, systemWeights.bold, styles.authLogo]}>Articles</Text>

       {this.state.loaded ?
         <FlatList style={{marginVertical: 15 }}
          data={this.state.list}
          showsVerticalScrollIndicator={false}
          keyExtractor={(item, index) => item.title}
          renderItem={({ item, index }) => (

          <View style={styles.articleContainer}>
        <TouchableNativeFeedback
         activeOpacity={0.8} onPress={() => navigate('BlogPost',
         { title: item.title, description: item.description }) } >
         <Text style={[human.subhead, systemWeights.bold]}>{item.title}</Text>
        </TouchableNativeFeedback>
          </View>

          )}/>
        : null }

  </View>
    );
  }
}

const { width, height } = Dimensions.get('window');
export default SpeakersScreen
