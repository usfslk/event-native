import React from 'react';
import { StyleSheet, View, FlatList, Text, Dimensions, Image, WebView, TouchableOpacity, StatusBar, Linking,
TouchableNativeFeedback, ImageBackground
} from 'react-native';
import ReactNativeParallaxHeader from 'react-native-parallax-header';
import {withNavigation} from 'react-navigation';
import Icon from 'react-native-vector-icons/Feather';
import { iOSColors, human, systemWeights } from 'react-native-typography';
import { Button, Avatar, Divider, Header} from 'react-native-elements';
import firebase from 'firebase';
import fire from '../config'
import styles from '../assets/styles';
import Timeline from 'react-native-timeline-listview'

class HomeScreen extends React.Component {
static navigationOptions = ({ navigation }) => {
return { header: null }}
constructor(props) {
  super(props)
  this.state = {
    loading: true,
  }
}

componentDidMount = () => {
  this.setState({loading: true})
  const { currentUser } = firebase.auth();
    firebase.database().ref(`/master/${currentUser.uid}/feed/sessions/`)
    .on('value', snapshot => {
    var obj = snapshot.val()
    var list = []
    var keys = []
    for(let a in obj){
        list.push(obj[a])
        keys.push(a)
    }
     this.setState({
          list:list,
          keys:keys,
          loading: false,
          loaded: true,
  }, () => {
    if (this.state.list.length === 0)
        this.setState({loading: false, empty: true})
     });
   });
}

render() {
  return (
  <View style={styles.container}>

    <Text style={[human.subhead, systemWeights.bold, styles.authLogo]}>Agenda</Text>

    {this.state.empty ?
    <Text style={[human.subhead, systemWeights.bold]}>
      Such empty!
    </Text>
    : null }

       {this.state.loaded ?
         <FlatList
          style={{marginBottom: 12.5 }}
          data={this.state.list}
          showsVerticalScrollIndicator={false}
          keyExtractor={(item, index) => item.sessionTitle}
          renderItem={({ item, index }) => (

            <View style={styles.timeline} >
              <View style={styles.dateRow}>
                <Text style={[human.footnote, styles.white]}>{item.startdate}</Text>
              </View>
              <View style={styles.sessionRow}>
                <Text style={[human.subhead, systemWeights.bold]}>{item.sessionTitle}</Text>
                <Text style={[human.footnote, styles.justify]}>{item.sessionDescription}</Text>
              </View>
            </View>

         )}/>
      : null }

  </View>
    );
  }
}

const { width, height } = Dimensions.get('window');
export default HomeScreen
