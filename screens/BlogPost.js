import React from 'react';
import { StyleSheet, View, FlatList, Text, Dimensions, Image, WebView, TouchableOpacity, StatusBar, Linking,
TouchableNativeFeedback, ImageBackground
} from 'react-native';
import ReactNativeParallaxHeader from 'react-native-parallax-header';
import {withNavigation} from 'react-navigation';
import Icon from 'react-native-vector-icons/Feather';
import { iOSColors, human, systemWeights } from 'react-native-typography';
import { Button, Avatar, Divider, Header, } from 'react-native-elements';
import firebase from 'firebase';
import fire from '../config'
import styles from '../assets/styles';
import Accordion from 'react-native-collapsible/Accordion';

class BlogPost extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: `${navigation.state.params.title}`,
      headerStyle: {  height: 60, backgroundColor: iOSColors.pink, elevation:0},
      headerTitleStyle: { color: '#F6F6F6', textAlign: 'center', fontWeight: '600'},
      headerLeft:
      <TouchableOpacity style={{paddingLeft: 25}}
      onPress={ () => { navigation.navigate('ArticleScreen') }} >
      <Icon name='arrow-left' style={{ fontSize: 20, color: '#fff', paddingRight: 25 }} />
      </TouchableOpacity>
    }
  };

  constructor(props) {
    super(props)
    this.state = {
      loading: true,
      more: false,
    }
  }


componentDidMount = () => {

}

more = () => {
  this.setState({more:!this.state.more})
}

render() {
  const { navigate } = this.props.navigation;
  return (
    <View style={styles.container}>
      <View style={styles.blog}>
         <Text style={[human.subhead, styles.justify]}>{this.props.navigation.state.params.description}</Text>
    </View>
  </View>
    );
  }
}

const { width, height } = Dimensions.get('window');
export default BlogPost;
