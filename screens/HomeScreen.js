import React from 'react';
import { StyleSheet, View, FlatList, Text, Dimensions, Image, ScrollView, TouchableOpacity, StatusBar, Linking,
TouchableNativeFeedback, ImageBackground, WebView
} from 'react-native';
import ReactNativeParallaxHeader from 'react-native-parallax-header';
import {withNavigation} from 'react-navigation';
import Icon from 'react-native-vector-icons/Feather';
import { iOSColors, human, systemWeights } from 'react-native-typography';
import { Button, Avatar, Divider, Header, Card} from 'react-native-elements';
import firebase from 'firebase';
import fire from '../config';
import styles from '../assets/styles';
import Modal from 'react-native-modalbox';

class HomeScreen extends React.Component {
static navigationOptions = ({ navigation }) => {
return { header: null }}
constructor(props) {
  super(props)
  this.state = {
    loading: true,
    loggedIn: null,
    loggedOut: null
  }
}

componentDidMount = () => {
  this.setState({loading: true})
  const { currentUser } = fire.auth();
  this.setState({ userEmail: currentUser.email, fullname: currentUser.displayName, avatar: currentUser.photoURL })
  firebase.database().ref(`/master/${currentUser.uid}/setup/`)
    .on('value', snapshot => {
    var obj = snapshot.val()
    var list = []
    var keys = []
    for(let a in obj){
        list.push(obj[a])
        keys.push(a)
    }
     this.setState({
          list:list,
          keys:keys,
          loading: false,
          loaded: true,
  }, () => {
        if (this.state.list.length === 0)
            this.setState({loading: false, empty: true})
       });
  });
}

onLogOut = () => {
  const { navigate } = this.props.navigation;
  firebase.auth().signOut();
   navigate('Login')
};



render() {
  return (
    <View style={{flex:1}}>
      <Modal style={styles.modal} backdropOpacity={0} position={"top"} ref={"popup"} backButtonClose={true} entry={'top'} animationDuration={300}>
        <View style={styles.smalldivider} />
          <Text style={[human.largeTitle, styles.center, systemWeights.bold]}>We are delighted to have you join us in New York!</Text>
          <View style={styles.largedivider} />
          <Text style={human.subhead}>Ticket prices are in USD and include all transaction fees.
          We reserved 20 discounted tickets for students and nonprofit organisations —
          just send us an email at hello@futureconf.com and we’ll get back to you soon!</Text>
        <View style={styles.smalldivider} />
          <View style={styles.timerow}>
          <TouchableNativeFeedback>
                <View style={styles.cardPressed}>
                  <Text style={human.headline}>Early Bird</Text>
                  <Text style={human.footnote}>Sales end in 3 days</Text>
                </View>
          </TouchableNativeFeedback>
          <TouchableNativeFeedback>
                <View style={styles.cardPressed}>
                  <Text style={human.headline}>General Admission</Text>
                  <Text style={human.footnote}>Sales end in 5 days</Text>
                </View>
          </TouchableNativeFeedback>
          </View>
      </Modal>
      <ScrollView style={styles.container}>
      {this.state.loaded ?
      <ImageBackground
      source={{uri: this.state.list[2]}}
      blurRadius={3}
       style={{
             flex: 1,
             width: width,
             height: null,
             }} >

    <View style={styles.overlay} />

     {this.state.loading ?
       <View>
         <Text style={[styles.spinner, styles.white, systemWeights.bold]}>Loading ...</Text>
       </View>
     : null }

        <Text style={[human.title1, systemWeights.bold, styles.center, styles.white, style={paddingVertical: 25}]}>
          {this.state.list[6]}
        </Text>

       <Card
          containerStyle={styles.elementsCard}
          imageProps={{borderTopLeftRadius:8, borderTopRightRadius: 8}}
          image={{uri: this.state.list[2]}}>
          <Text style={[systemWeights.bold, human.title2, styles.pSmall]}>
            {this.state.list[0]}
          </Text>
          <Text style={styles.pSmall}>
          {this.state.list[4]} {this.state.list[5]}
          </Text>
          <Text style={[human.footnote, styles.justify, styles.pSmall]}>
            {this.state.list[1]}
          </Text>
        </Card>

          <Card
            containerStyle={styles.elementsCard}
            imageProps={{borderTopLeftRadius:8, borderTopRightRadius: 8}}
            image={{uri: 'https://static-maps.yandex.ru/1.x/?lang=en-US&ll=-122.447810,37.801520&z=13&l=map&size=600,300&pt=-122.447810,37.801520,flag'}}>
            <Text style={[human.subhead, systemWeights.bold, styles.pSmall]}>
              Location
            </Text>
            <Text style={[human.footnote, styles.pSmall]}>
              {this.state.list[3]}
            </Text>
          </Card>

       <Card
         containerStyle={styles.elementsCard}
         imageProps={{borderTopLeftRadius:8, borderTopRightRadius: 8}}
         image={{uri: 'https://githubuniverse.com/assets/img/GitHub-Universe-lounge.jpg'}}>
          <Text style={[human.title2, styles.pSmall]}>
            About
          </Text>
          <Text style={[human.footnote, styles.justify, styles.pSmall]}>
            In a world surrounded by modern technology,
            computing has a big impact on society. It has enabled most citizens who have access to communicate effectively,
            expressing their ideas and utilizing their creativity to the best of their ability.
          </Text>
            <View style={styles.smalldivider} />
          <View style={styles.rowsocial}>
            <Icon name="facebook" onPress={ ()=>{ Linking.openURL('https://usfslk.github.io')}} size={25} color={iOSColors.black} />
            <Icon name="twitter" onPress={ ()=>{ Linking.openURL('https://twitter.com/usfslk')}}  size={25} color={iOSColors.black} />
            <Icon name="instagram" onPress={ ()=>{ Linking.openURL('https://instagram.com/protobulb_io')}}  size={25} color={iOSColors.black} />
          </View>
       </Card>



       <TouchableNativeFeedback >
        <Button
          onPress={() => this.refs.popup.open()}
           title='Join'
           rounded backgroundColor={iOSColors.pink}
           containerViewStyle={styles.button}
           icon={{name: 'user-plus', type: 'feather'}}
           />
       </TouchableNativeFeedback>

       <View style={styles.btnRow}>
        <Button
          title='Contact'
          rounded backgroundColor={iOSColors.black}
          containerViewStyle={styles.button}
          icon={{name: 'mail', type: 'feather'}}
          onPress={() => Linking.openURL('mailto:ynakles@gmail.com') } />

          <Button rounded
            backgroundColor={iOSColors.black}
            containerViewStyle={styles.button}
            icon={{name: 'log-out', type: 'feather'}}
            title='Logout'
            onPress={this.onLogOut}
            />
       </View>

      </ImageBackground>
    : null }
  </ScrollView>
</View>
    );
  }
}



const { width, height } = Dimensions.get('window');
export default HomeScreen
