import React, {Component} from 'react';
import { StyleSheet, View, FlatList, Text, Dimensions, Image, ScrollView, TouchableNativeFeedback, StatusBar, ImageBackground} from 'react-native';
import { iOSColors, human, systemWeights } from 'react-native-typography'
import { Button, Avatar, Divider, Header, Icon} from 'react-native-elements'
import { Dropdown } from 'react-native-material-dropdown';
import Modal from 'react-native-modalbox';

export default class TicketScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      header: null }
  }

  constructor(props) {
      super(props)
      this.state = {
         option1: false,
         option2: false,
         option3: false,
         option4: false,
         checkout: false,
      }
  }

  option1 = () => {
    this.setState({option1: !this.state.option1})
  }
  option2 = () => {
    this.setState({option2: !this.state.option2})
  }
  option3 = () => {
    this.setState({option3: !this.state.option3})
  }
  option4 = () => {
    this.setState({option4: !this.state.option4})
  }
  checkout = () => {
    this.setState({checkout: true})
    this.refs.modal.close()
  }

  render() {
      let data = [{ value: '1' }, { value: '2' }, { value: '3' }, { value: '4' }, { value: '5' },
      { value: '6' }, { value: '7' }, { value: '8' }, { value: '9' }, { value: '10' }];
      const { navigate } = this.props.navigation;
      return (
      <View style={{flex:1}}>
      <Header
        backgroundColor={iOSColors.pink}
        statusBarProps={{ barStyle: 'light-content' }}
        leftComponent={<Icon name="arrow-left" type='feather' size={25} color={'#fff'} onPress={() =>
                     navigate('HomeScreen')
                     } />}
        rightComponent={<Icon name="more-vert" type='MaterialIcons' size={25} color={'#fff'} onPress={() => this.refs.popup.open()}/>}
        outerContainerStyles={{ minHeight: 0, borderBottomWidth: 0, paddingTop: StatusBar.currentHeight}}
      />

      <View style={styles.container}>

        <Modal style={styles.modal} backdropOpacity={0} style={styles.popupblock} position={"top"} ref={"popup"} backButtonClose={true} entry={'top'} animationDuration={300}>
          <View style={styles.smalldivider} />
              <View style={styles.popuprow}>
                  <Icon name="clock" type='feather' size={20} color={iOSColors.black} />
                  <Text style={styles.popup}>Add to calendar</Text>
              </View>
              <View style={styles.popuprow}>
                  <Icon name="flag" type='feather' size={20} color={iOSColors.black} />
                  <Text style={styles.popup}>Report event</Text>
              </View>
          <View style={styles.smalldivider} />
        </Modal>

        <Modal style={styles.modal} backdropOpacity={0} position={"bottom"} ref={"modal"} backButtonClose={true} swipeArea={20} animationDuration={300}>
          <ScrollView>

              <Text style={human.headline}>Conference Tickets</Text>
                <View style={styles.smalldivider} />

                <View style={styles.row}>

                <View style={!this.state.option1 ? styles.card : styles.cardPressed }>
                    <Text style={human.headline}>FutureConf NY Early Bird</Text>
                    <Text style={human.footnote}>Sold Out</Text>
                  </View>

                <TouchableNativeFeedback onPress={this.option2}>
                <View style={!this.state.option2 ? styles.card : styles.cardPressed }>
                    <Text style={human.headline}>FutureConf NY Ticket</Text>
                    <Text style={human.footnote}>$99</Text>
                  </View>
                </TouchableNativeFeedback>

                <TouchableNativeFeedback onPress={this.option3}>
                <View style={!this.state.option3 ? styles.card : styles.cardPressed }>
                    <Text style={human.headline}>Conference + Workshop Ticket</Text>
                    <Text style={human.footnote}>$149</Text>
                  </View>
                </TouchableNativeFeedback>

                <TouchableNativeFeedback onPress={this.option4}>
                  <View style={!this.state.option4 ? styles.card : styles.cardPressed }>
                    <Text style={human.headline}>Conference + 2 Workshop Tickets</Text>
                    <Text style={human.footnote}>$199</Text>
                  </View>
                </TouchableNativeFeedback>

                </View>

              <Dropdown containerStyle={{width: width}}
              label='Qty'
              data={data}
              />

              <View style={styles.smalldivider} />

              <Button rounded
                backgroundColor={iOSColors.pink}
                containerViewStyle={styles.button}
                title='Checkout'
                onPress={this.checkout}
              />

          </ScrollView>
        </Modal>

        <ImageBackground
        source={require('../assets/images/ticketbg.jpg')}
         style={{
               width: width,
               height: height,
               alignItems: 'center',
               justifyContent: 'center',
               paddingHorizontal: 25}} >

        {this.state.checkout ?
           <Text style={[human.headline, styles.chechout]}>Thanks for your order!</Text>
        : null }

        {!this.state.checkout ?
          <View>

          <View style={styles.smalldivider} />

        <Text style={[human.largeTitle, styles.center, systemWeights.bold]}>We are delighted to have you join us in New York!</Text>
        <View style={styles.largedivider} />
        <Text style={human.subhead}>Ticket prices are in USD and include all transaction fees.
        We reserved 20 discounted tickets for students and nonprofit organisations —
        just send us an email at hello@futureconf.com and we’ll get back to you soon!</Text>

        <View style={styles.largedivider} />

        <View style={styles.timerow}>

        <TouchableNativeFeedback onPress={() => this.refs.modal.open()}>
              <View style={styles.card}>
                <Text style={human.headline}>Early Bird</Text>
                <Text style={human.footnote}>Sales end in 3 days</Text>
              </View>
        </TouchableNativeFeedback>
        <TouchableNativeFeedback onPress={() => this.refs.modal.open()}>
              <View style={styles.card}>
                <Text style={human.headline}>General Admission</Text>
                <Text style={human.footnote}>Sales end in 5 days</Text>
              </View>
        </TouchableNativeFeedback>

        </View>

        </View> : null }

        </ImageBackground>
      </View>
    </View>
      );
    }
  }
