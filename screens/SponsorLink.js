import React from 'react';
import { StyleSheet, View, FlatList, Text, Dimensions, Image, WebView, TouchableOpacity, StatusBar, Linking,
TouchableNativeFeedback, ImageBackground,
} from 'react-native';
import ReactNativeParallaxHeader from 'react-native-parallax-header';
import {withNavigation} from 'react-navigation';
import Icon from 'react-native-vector-icons/Feather';
import { iOSColors, human, systemWeights } from 'react-native-typography';
import { Button, Avatar, Divider, Header, } from 'react-native-elements';
import firebase from 'firebase';
import fire from '../config'
import styles from '../assets/styles';
import Accordion from 'react-native-collapsible/Accordion';

class SponsorLink extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: `${navigation.state.params.sponsor}`,
      headerStyle: {  height: 60, backgroundColor: iOSColors.pink, elevation:0},
      headerTitleStyle: { color: '#F6F6F6', textAlign: 'center', fontWeight: '600'},
      headerLeft:
      <TouchableOpacity style={{paddingLeft: 25}}
      onPress={ () => { navigation.navigate('SponsorScreen') }} >
      <Icon name='arrow-left' style={{ fontSize: 20, color: '#fff', paddingRight: 25 }} />
      </TouchableOpacity>
    }
  };

render() {
  const { navigate } = this.props.navigation;
  return (
    <View style={{flex:1}}>
      <WebView source={{uri: `${this.props.navigation.state.params.link}`}} />
  </View>
    );
  }
}

const { width, height } = Dimensions.get('window');
export default SponsorLink;
