import React from 'react';
import { StyleSheet, View, FlatList, Text, Dimensions, Image, WebView, TouchableOpacity, StatusBar, Linking,
TouchableNativeFeedback, ImageBackground
} from 'react-native';
import ReactNativeParallaxHeader from 'react-native-parallax-header';
import {withNavigation} from 'react-navigation';
import Icon from 'react-native-vector-icons/Feather';
import { iOSColors, human, systemWeights } from 'react-native-typography';
import { Button, Avatar, Divider, Header, ListItem} from 'react-native-elements';
import firebase from 'firebase';
import fire from '../config'
import styles from '../assets/styles';

class SpeakersScreen extends React.Component {
static navigationOptions = ({ navigation }) => {
  return { header: null }}

  constructor(props) {
    super(props)
    this.state = {
      loading: true,
    }
  }


componentDidMount = () => {
this.setState({loading: true})
const { currentUser } = firebase.auth();
  firebase.database().ref(`/master/${currentUser.uid}/feed/speakers/`)
  .on('value', snapshot => {
  var obj = snapshot.val()
  var list = []
  var keys = []
  for(let a in obj){
      list.push(obj[a])
      keys.push(a)
  }
   this.setState({
        list:list,
        keys:keys,
        loading: false,
        loaded: true,
}, () => {
  if (this.state.list.length === 0)
      this.setState({loading: false, empty: true})
 });
});
}



render() {
  return (
    <View style={styles.container}>
      <Text style={[human.subhead, systemWeights.bold, styles.authLogo]}>Speakers</Text>
        {this.state.loaded ?
         <FlatList style={{marginVertical: 15 }}
          data={this.state.list}
          showsVerticalScrollIndicator={false}
          keyExtractor={(item, index) => item.description}
          renderItem={({ item, index }) => (

          <View style={[styles.row, styles.speakerRow]}>

            <View style={{flex: 0, flexShrink: 1}}>
              <Text style={human.title3}>{item.name}</Text>
              <Text style={human.footnone}>{item.company}</Text>
              <Text style={human.footnone}>{item.description}</Text>
            </View>

            <View>
              <Avatar
                size="small" rounded
                source={{uri: item.avatar}} />
            </View>

          </View>

          )}/>
        : null }

  </View>
    );
  }
}

const viewImages = { background: require('../assets/images/bg.jpg')};
const { width, height } = Dimensions.get('window');

export default SpeakersScreen
