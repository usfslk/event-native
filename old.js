renderContent() {
  return (
    <View style={{paddingHorizontal: 50, paddingVertical: 25}}>

    <Text style={human.largeTitle}>AI & Society</Text>
    <Text style={human.subhead}>by Think Coffee</Text>

    <View style={styles.largedivider} />

    <View style={styles.row}>
      <View>
        <Icon name="calendar" size={25} color={iOSColors.pink} />
      </View>
      <View style={{paddingLeft: 25}}>
        <Text style={human.headline}>Wednesday 26, September 2018</Text>
        <Text style={human.footnote}>4:00 PM - 9:00 PM EDT</Text>
      </View>
    </View>

    <View style={styles.row}>
      <View>
        <Icon name="map-pin" size={25} color={iOSColors.pink} />
      </View>
      <View style={{paddingLeft: 25}}>
        <Text style={human.headline}>University of Luxembourg</Text>
        <Text style={human.footnote}>4328 Esch-sur-Alzette, Luxembourg</Text>
      </View>
    </View>

    <View style={styles.row}>
      <View>
        <Icon name="info" size={25} color={iOSColors.pink} />
      </View>
      <View style={{paddingLeft: 25}}>
        <Text style={human.headline}>Free</Text>
        <Text style={human.footnote}>on Eventbrite</Text>
      </View>
    </View>

    <View style={styles.smalldivider} />

    <View style={styles.timerow}>
      <View style={styles.card}>
        <Text style={human.headline}>Sep 26</Text>
        <Text style={human.footnote}>WED 4:00 PM</Text>
      </View>
      <View style={styles.card}>
        <Text style={human.headline}>Sep 27</Text>
        <Text style={human.footnote}>THU 4:00 PM</Text>
      </View>
      <View style={styles.card}>
        <Text style={human.headline}>Sep 28</Text>
        <Text style={human.footnote}>FRI 4:00 PM</Text>
      </View>
    </View>

    <View style={styles.smalldivider} />

        <Text style={[human.footnote, styles.justify]}>Social Project Coffee is based on the idea that it is the efforts we make every day to help farm workers or their communities that make the biggest difference.
        Claims by coffee roasters of paying prices above market are nice, but the higher prices paid for quality coffee –
        and marketed by roasters as a social good, often end up in the pockets of landed farmers who may or may not share their wealth with those who really need it. </Text>

        <View style={styles.smalldivider} />
        <Text onPress={() => navigate('DetailsScreen')} style={[human.subhead, systemWeights.bold, styles.learnmore]}>Learn more</Text>

        <View style={styles.smalldivider} />
        <Text style={human.title2}>Location</Text>
        <Text>2414 E Sunrise Blvd, Fort Lauderdale, FL 33304, USA</Text>
        <View style={styles.smalldivider} />

        <View style={{flex: 1, flexDirection:'column'}}>

        <WebView
           originWhitelist={['*']} scrollEnabled={false}
           source={{ html: '<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0"><div style="text-align: center"><img width="350" src="https://maps.googleapis.com/maps/api/staticmap?center=fort+lauderdale&zoom=13&scale=1&size=350x200&maptype=roadmap&format=png&visual_refresh=true" alt="Google Map of fort lauderdale"></div>' }}
           style={{ height: 220, backgroundColor: 'transparent', marginBottom: 12.5}}
         />

        <View style={styles.smalldivider} />
       </View>

      <View style={styles.rowprofile}>
        <Text style={[human.title2]}>About</Text>
        <Avatar small rounded
        source={{uri: "https://images.unsplash.com/photo-1496345875659-11f7dd282d1d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=a0f994eef47e5fb1a67849703cc961b3&auto=format&fit=crop&w=1050&q=80"}}
        onPress={() => console.log("Works!")}
        activeOpacity={0.7}
        />
      </View>
      <View style={styles.smalldivider} />
       <Text style={[human.footnote, styles.justify]}>In a world surrounded by modern technology,
       computing has a big impact on society. It has enabled most citizens who have access to communicate effectively,
       expressing their ideas and utilizing their creativity to the best of their ability.</Text>

       <View style={styles.largedivider} />

       <View style={styles.rowsocial}>
         <Icon name="facebook" onPress={ ()=>{ Linking.openURL('https://usfslk.github.io')}} size={25} color={iOSColors.black} />
         <Icon name="twitter" onPress={ ()=>{ Linking.openURL('https://twitter.com/usfslk')}}  size={25} color={iOSColors.black} />
         <Icon name="instagram" onPress={ ()=>{ Linking.openURL('https://instagram.com/protobulb_io')}}  size={25} color={iOSColors.black} />
       </View>

      <View style={styles.largedivider} />

          <Button rounded
            backgroundColor={iOSColors.pink}
            containerViewStyle={styles.button}
            icon={{name: 'user-plus', type: 'feather'}}
            title='Join'
            onPress={() => navigate('TicketScreen')}
          />

         <View style={styles.smalldivider} />

         <Button rounded
           backgroundColor={iOSColors.black}
           containerViewStyle={styles.button}
           icon={{name: 'mail', type: 'feather'}}
           title='Contact'
           onPress={() => Linking.openURL('mailto:support@example.com') } />

    </View>
  );
}
