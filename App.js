import React, { Component } from "react";
import { createMaterialTopTabNavigator, createSwitchNavigator, createStackNavigator } from 'react-navigation';
import {StatusBar, Dimensions, } from 'react-native'
import Icon from 'react-native-vector-icons/Feather';
import { iOSColors, human, systemWeights } from 'react-native-typography';
import { Button, Avatar, Divider, Header} from 'react-native-elements';

import HomeScreen from './screens/HomeScreen';
import DetailsScreen from './screens/DetailsScreen';
import TicketScreen from './screens/TicketScreen';
import SessionScreen from './screens/SessionScreen';
import SpeakersScreen from './screens/SpeakersScreen';
import ArticleScreen from './screens/ArticleScreen';
import SponsorScreen from './screens/SponsorScreen';
import WebScreen from './screens/WebScreen';
import Login from './screens/Login';
import BlogPost from './screens/BlogPost';
import SponsorLink from './screens/SponsorLink';
import WebLink from './screens/WebLink';

const AppStack =
createMaterialTopTabNavigator(  {
    HomeScreen: {
      screen: HomeScreen
    },
    SessionScreen: {
      screen: SessionScreen
    },
    SpeakersScreen: {
      screen: SpeakersScreen
    },
    ArticleScreen: {
      screen: ArticleScreen
    },
    SponsorScreen:{
      screen: SponsorScreen
    },
    WebScreen:{
      screen: WebScreen
    },
  },
    {
      initialRouteName: 'ArticleScreen',
      swipeEnabled: true,
      animationEnabled: false,
      lazy:false,
      optimizationsEnabled: true,

      tabBarOptions: {
        activeTintColor: iOSColors.white,
        inactiveTintColor: iOSColors.gray,
        upperCaseLabel: false,
        scrollEnabled:false,
        allowFontScaling: true,
        showIcon: true,
        showLabel: false,
        style: {
          paddingTop: StatusBar.currentHeight,
          backgroundColor: iOSColors.pink,
          elevation: 0
        },
        indicatorStyle: {
          backgroundColor: iOSColors.white
        }
      }
    }
);


;
const AuthStack = createStackNavigator({ Login:Login });
const BlogStack = createStackNavigator({ BlogPost:BlogPost });
const SponsorStack = createStackNavigator({ SponsorLink:SponsorLink });
const WebStack = createStackNavigator({ WebLink:WebLink });

export default createSwitchNavigator(
  {
    App: AppStack,
    Auth: AuthStack,
    Blog: BlogStack,
    Sponsor: SponsorStack,
    Web: WebStack,
  },
  {
    initialRouteName: 'Auth',
  }
);


HomeScreen.navigationOptions = {
  tabBarColor: iOSColors.black,
  tabBarIcon: ({ tintColor, focused }) => (
    <Icon size={20}  name={'home'} color={iOSColors.white} />
  )
};

SessionScreen.navigationOptions = {
  tabBarColor: iOSColors.black,
  tabBarIcon: ({ tintColor, focused }) => (
    <Icon size={20}  name={'calendar'} color={iOSColors.white} />
  )
};


SpeakersScreen.navigationOptions = {
  tabBarColor: iOSColors.black,
  tabBarIcon: ({ tintColor, focused }) => (
    <Icon size={20}  name={'mic'} color={iOSColors.white} />
  )
};

ArticleScreen.navigationOptions = {
  tabBarColor: iOSColors.black,
  tabBarIcon: ({ tintColor, focused }) => (
    <Icon size={20}  name={'edit-2'} color={iOSColors.white} />
  )
};

SponsorScreen.navigationOptions = {
  tabBarColor: iOSColors.black,
  tabBarIcon: ({ tintColor, focused }) => (
    <Icon size={20}  name={'users'} color={iOSColors.white} />
  )
};

WebScreen.navigationOptions = {
  tabBarColor: iOSColors.black,
  tabBarIcon: ({ tintColor, focused }) => (
    <Icon size={20}  name={'globe'} color={iOSColors.white} />
  )
};



const { width, height } = Dimensions.get('window');
